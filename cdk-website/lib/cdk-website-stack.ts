import * as cdk from 'aws-cdk-lib';
import {CfnOutput} from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {BucketDeployment, Source} from "aws-cdk-lib/aws-s3-deployment";
import {BlockPublicAccess, Bucket} from "aws-cdk-lib/aws-s3";

export class CdkWebsiteStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const destinationBucket = new Bucket(this, `cdk-website-s3-bucket`, {
      publicReadAccess: true,
      websiteIndexDocument: 'index.html',
      blockPublicAccess: BlockPublicAccess.BLOCK_ACLS,
    })

    new BucketDeployment(this, 'cdk-website-s3-deployment', {
      sources: [Source.asset('./build')],
      destinationBucket,
    });

    new CfnOutput(this, 'cdk-website-url', {
      value: destinationBucket.bucketWebsiteUrl
    })

  }
}
